﻿namespace SpecFlowPOC
{
    public class SpecFlowCalculator
    {
        public int FirstNumber { get; set; }
        public int SecondNumber { get; set; }

        public int Add()
        {
            return FirstNumber + SecondNumber;
        }

    }
}