using SpecFlowSelenium.Entities;
using SpecFlowSelenium.Pages;

namespace SpecFlowSelenium.StepDefinitions
{
    [Binding]
    public class ShoppingCartStepDefinitions
    {
        private readonly ProductPage _productPage;
        private readonly ShoppingCartPage _shoppingCartPage;
        private IList<Product> _addedProducts;

        public ShoppingCartStepDefinitions(ProductPage productPage, ShoppingCartPage shoppingCartPage)
        {
            _productPage = productPage;
            _shoppingCartPage = shoppingCartPage;
        }

        [When(@"I add (.*) item to my shopping cart")]
        public void WhenIAddItemToMyShoppingCart(int amount)
        {
            _addedProducts = _productPage.Products.AddToCart(amount);
        }

        [Then(@"(.*) item should be in the shopping cart")]
        public void ThenItemShouldBeInTheShoppingCart(int amount)
        {
            _productPage.Header.ShoppingCart.ItemCount.Should().Be(amount);
        }

        [When(@"I go to my shopping cart")]
        public void WhenIGoToMyShoppingCart()
        {
            _productPage.Header.ShoppingCart.Visit();
        }

        [Then(@"I should find that item")]
        public void ThenIShouldFindThatItem()
        {
            _shoppingCartPage.Items.First.Title.Should().Be(_addedProducts[0].Title);
        }

    }
}
