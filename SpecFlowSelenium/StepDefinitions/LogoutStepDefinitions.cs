using SpecFlowSelenium.Entities;
using SpecFlowSelenium.Pages;

namespace SpecFlowSelenium.StepDefinitions
{
    [Binding]
    public class LogoutStepDefinitions
    {
        private readonly LoginPage _loginPage;

        private readonly ProductPage _productPage;

        public LogoutStepDefinitions(LoginPage loginPage, ProductPage productPage)
        {
            _loginPage = loginPage;
            _productPage = productPage;
        }

        [Given(@"I am logged in")]
        public void GivenIAmLoggedIn()
        {
            _loginPage
                .NavigateToLoginPage()
                .ProvideCredentials(Users.StandardUser.Username, Users.StandardUser.Password)
                .ClickLoginButton()
                .VerifyOnProductPage();
        }

        [When(@"I log out")]
        public void WhenILogOut()
        {

            _productPage.Header.Logout();
        }

        [Then(@"My session should be terminated")]
        public void ThenMySessionShouldBeTerminated()
        {
            _loginPage.VerifyLoggedOut();
        }
    }
}
