using SpecFlowSelenium.Pages;

namespace SpecFlowSelenium.StepDefinitions
{
    [Binding]
    internal class LoginStepDefinitions
    {

        private readonly LoginPage _loginPage;

        private readonly ProductPage _productPage;

        public LoginStepDefinitions(LoginPage loginPage, ProductPage productPage)
        {
            this._loginPage = loginPage;
            this._productPage = productPage;
        }


        [Given(@"I navigate to the homepage")]
        public void GivenINavigateToTheHomepage()
        {
            _loginPage.NavigateToLoginPage();
        }

        [Given(@"I provide valid credentials")]
        public void GivenIProvideValidCredentials()
        {
            _loginPage.ProvideCredentials("standard_user", "secret_sauce");
        }

        [When(@"I login")]
        public void WhenILogin()
        {
            _loginPage.ClickLoginButton();
        }

        [Then(@"I should see the products page")]
        public void ThenIShouldSeeTheProductsPage()
        {
            _productPage.VerifyOnProductPage();
        }
    }
}
