﻿using OpenQA.Selenium;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Pages
{
    public class LoginPage : PageBase
    {

        public LoginPage(IWebDriver driver) : base(driver) { }

        public LoginPage NavigateToLoginPage()
        {
            driver.Navigate().GoToUrl("https://www.saucedemo.com/");
            return this;
        }

        public LoginPage ProvideCredentials(string username, string password)
        {
            IWebElement loginInput = driver.FindElement(By.Id("user-name"));
            loginInput.SendKeys(username);
            IWebElement passwordInput = driver.FindElement(By.Id("password"));
            passwordInput.SendKeys(password);
            return this;
        }

        public ProductPage ClickLoginButton()
        {
            IWebElement loginButton = driver.FindElement(By.Id("login-button"));
            loginButton.Click();
            return new ProductPage(driver);
        }

        public LoginPage VerifyLoggedOut()
        {
            driver.Manage().Cookies.GetCookieNamed("session-username").Should().BeNull();
            return this;
        }
    }
}
