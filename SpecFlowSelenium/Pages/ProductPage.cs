﻿using OpenQA.Selenium;
using SpecFlowSelenium.Entities;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Pages
{
    public class ProductPage : PageBase
    {

        public Header Header { get => new(driver); }

        public Products Products { get => new(driver); }

        public ProductPage(IWebDriver driver) : base(driver) { }

        public ProductPage VerifyOnProductPage()
        {
            wait.Until(d => d.Url.EndsWith("inventory.html"));
            return this;
        }
    }
}
