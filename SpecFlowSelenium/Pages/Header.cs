﻿using OpenQA.Selenium;
using SpecFlowSelenium.Entities;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Pages
{
    public class Header : PageBase
    {

        public ShoppingCart ShoppingCart { get => new(driver); }

        public Header(IWebDriver driver) : base(driver) { }

        public LoginPage Logout()
        {
            By hamburgerMenu = By.Id("react-burger-menu-btn");
            ClickElement(hamburgerMenu);

            By logoutLink = By.Id("logout_sidebar_link");
            ClickElement(logoutLink);

            return new LoginPage(driver);
        }
    }
}
