﻿using OpenQA.Selenium;
using SpecFlowSelenium.Entities;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Pages
{
    public class ShoppingCartPage : PageBase
    {
        public Items Items => new(driver);

        public ShoppingCartPage(IWebDriver driver) : base(driver) { }

        public ShoppingCartPage VerifyOnShoppingCartPage()
        {
            wait.Until(d => d.Url.EndsWith("cart.html"));
            return this;
        }

    }
}
