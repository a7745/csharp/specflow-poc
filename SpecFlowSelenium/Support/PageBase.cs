﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SpecFlowSelenium.Support
{
    public abstract class PageBase
    {
        protected readonly IWebDriver driver;

        protected readonly WebDriverWait wait;

        public PageBase(IWebDriver driver)
        {
            this.driver = driver;
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        public void ClickElement(By locatedBy)
        {
            wait.Until(d => d.FindElement(locatedBy).Displayed);
            driver.FindElement(locatedBy).Click();
        }
    }
}
