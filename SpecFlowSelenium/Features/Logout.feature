﻿Feature: Logout

As a user, I want to log out of the application.

@Logout
Scenario: Normal logout
	Given I am logged in
	When I log out
	Then My session should be terminated
