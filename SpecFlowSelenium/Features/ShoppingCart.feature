﻿Feature: Shopping Cart
As a user, I want to add items to and remove them from my shopping cart.

@ShoppingCart
Scenario: Add item to shopping cart
	Given I am logged in
	When I add <amount> item to my shopping cart
	Then <amount> item should be in the shopping cart
	Examples: 
	| amount |
	| 1      |
	| 2      |

@ShoppingCart
Scenario: Find item in shopping cart after adding them
	Given I am logged in
	When I add 1 item to my shopping cart
	And I go to my shopping cart
	Then I should find that item
