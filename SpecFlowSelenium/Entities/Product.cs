﻿using OpenQA.Selenium;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Entities
{
    public class Product : PageBase
    {
        public string? Title { get; set; }

        public string? Description { get; set; }

        public Product(IWebDriver driver) : base(driver) { }

        public void AddToCart()
        {
            IWebElement item = driver.FindElement(By.XPath($"//*[.='{Title}']/ancestor::*[@class='inventory_item']"));
            IWebElement addButton = item.FindElement(By.XPath("//*[.='Add to cart']"));
            addButton.Click();
        }
    }
}
