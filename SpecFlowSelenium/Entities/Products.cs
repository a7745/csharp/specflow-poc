﻿using OpenQA.Selenium;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Entities
{
    public class Products : PageBase
    {
        private readonly IList<Product> _products;

        public Products(IWebDriver driver) : base(driver)
        {
            _products = GetProducts();
        }

        public Product First => _products.First();

        public IList<Product> AddToCart(int amount)
        {
            IList<Product> addedProducts = new List<Product>();
            for (int i = 0; i < amount; i++)
            {
                _products[i].AddToCart();
                addedProducts.Add(_products[i]);
            }
            return addedProducts;
        }

        private IList<Product> GetProducts()
        {
            IList<Product> products = new List<Product>();
            wait.Until(d => d.FindElements(By.ClassName("inventory_item")).Count > 0);
            IReadOnlyCollection<IWebElement> items = driver.FindElements(By.ClassName("inventory_item"));
            foreach (IWebElement item in items)
            {
                Product product = new(driver)
                {
                    Title = item.FindElement(By.ClassName("inventory_item_name")).Text,
                    Description = item.FindElement(By.ClassName("inventory_item_desc")).Text
                };
                products.Add(product);
            }
            return products;
        }
    }
}
