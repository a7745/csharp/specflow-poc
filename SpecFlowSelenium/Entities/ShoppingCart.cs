﻿using OpenQA.Selenium;
using SpecFlowSelenium.Pages;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Entities
{
    public class ShoppingCart : PageBase
    {
        private int _itemCount;
        public int ItemCount
        {
            get
            {
                int.TryParse(driver.FindElement(By.ClassName("shopping_cart_badge")).Text, out _itemCount);
                return _itemCount;
            }
        }

        public ShoppingCart(IWebDriver driver) : base(driver) { }

        public ShoppingCartPage Visit()
        {
            By shoppingCartLink = By.ClassName("shopping_cart_link");
            ClickElement(shoppingCartLink);

            return new ShoppingCartPage(driver)
                .VerifyOnShoppingCartPage();
        }
    }
}