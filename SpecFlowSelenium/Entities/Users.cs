﻿namespace SpecFlowSelenium.Entities
{
    public class Users
    {
        public static User StandardUser { get; } = new("standard_user", "secret_sauce");
    }

    public class User
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public User(string username, string password)
        {
            Username = username;
            Password = password;
        }
    }
}
