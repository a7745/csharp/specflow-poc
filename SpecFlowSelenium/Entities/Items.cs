﻿using OpenQA.Selenium;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Entities
{
    public class Items : PageBase
    {
        private readonly IList<Item> _items;

        public Items(IWebDriver driver) : base(driver)
        {
            _items = GetItems();
        }

        public Item First => _items[0];

        private IList<Item> GetItems()
        {
            IList<Item> items = new List<Item>();
            wait.Until(d => d.FindElements(By.ClassName("cart_item")).Count > 0);
            IReadOnlyCollection<IWebElement> elements = driver.FindElements(By.ClassName("cart_item"));
            foreach (IWebElement element in elements)
            {
                Item item = new(driver)
                {
                    Title = element.FindElement(By.ClassName("inventory_item_name")).Text,
                    Description = element.FindElement(By.ClassName("inventory_item_desc")).Text
                };
                items.Add(item);
            }
            return items;
        }
    }
}
