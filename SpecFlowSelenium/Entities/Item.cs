﻿using OpenQA.Selenium;
using SpecFlowSelenium.Support;

namespace SpecFlowSelenium.Entities
{
    public class Item : PageBase
    {
        public string? Title { get; set; }

        public string? Description { get; set; }

        public Item(IWebDriver driver) : base(driver) { }

    }
}
