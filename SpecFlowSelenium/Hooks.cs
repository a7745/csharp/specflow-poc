﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace SpecFlowSelenium
{
    [Binding]
    public sealed class Hooks
    {
        private readonly IObjectContainer objectContainer;

        private IWebDriver? driver;

        public Hooks(IObjectContainer objectContainer)
        {
            this.objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void InitializeWebDriver()
        {
            driver = new ChromeDriver();
            objectContainer.RegisterInstanceAs<IWebDriver>(driver);
        }

        [AfterScenario]
        public void CleanupWebDriver()
        {
            driver?.Quit();
        }
    }
}